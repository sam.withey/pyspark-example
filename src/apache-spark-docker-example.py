#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pyspark
from pyspark.sql import SparkSession
import random


# In[2]:


"""The entry point to programming Spark with the Dataset and DataFrame API."""
spark_session = SparkSession.builder.appName('PySpark DataFrame From RDD').getOrCreate()


# In[3]:


"""Main entry point for Spark functionality. 
A SparkContext represents the connection to a Spark cluster, 
and can be used to create RDD and broadcast variables on that cluster.

Get or instantiate a SparkContext and register it as a singleton object."""
spark_context = pyspark.SparkContext.getOrCreate()


# In[4]:


print(type(spark_session))
print(type(spark_context))


# In[5]:


"""Generating 1000 number range using SparkContext RDD"""
num_list_rdd = spark_context.range(1000)
print(num_list_rdd.top(10))
"""Generating 1000 number range using SparkSession DataFrame"""
num_list_df = spark_session.range(1000).toDF("number")
print(num_list_df.head(10))


# In[6]:


"""Example creating a SparkSession DataFrame from a SparkContext RDD"""
rdd = spark_context.parallelize([('C',85,76,87,91), ('B',85,76,87,91), ("A", 85,78,96,92), ("A", 92,76,89,96)], 4)
sub = ['Division','English','Mathematics','Physics','Chemistry']
# Creates a DataFrame from an RDD, a list or a pandas.DataFrame.
sdf = spark_session.createDataFrame(data=rdd, schema=sub)
print(type(sdf))
sdf.printSchema()


# In[7]:


"""Example of getting all even numbers from SparkContext RDD"""
divis_by_2_rdd = num_list_rdd.filter(lambda x: x % 2 == 0)
print(divis_by_2_rdd.top(5))
"""Example of getting all even numbers from SparkSession DataFrame using where method"""
divis_by_2_df_where = num_list_df.where("number % 2 = 0")
print(divis_by_2_df_where.head(5))
"""Example of getting all even numbers from SparkSession DataFrame using filter method"""
divis_by_2_df_filter = num_list_df.filter("number % 2 = 0")
print(divis_by_2_df_where.head(5))

